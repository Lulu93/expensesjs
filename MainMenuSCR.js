/* MENU NAVIGATION */

$("#btnExpenses").click(function() {
	$("#pgOverview").fadeOut();
	$("#pgExpenses").fadeIn();
	$("#btnExpenses").addClass("active");
	$("#btnOverview").removeClass("active");
});

$("#btnOverview").click(function() {
	$("#pgExpenses").fadeOut();
	$(".bodyContainer").fadeOut();
	$("#pgOverview").fadeIn();
	$("#btnOverview").addClass("active");
	$("#btnExpenses").removeClass("active");
});