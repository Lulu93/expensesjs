/* TICKET FUNCTIONS */
  
$( ".openTicket").click(function() {
	$(this).parent().parent().siblings().toggle();
});

$( ".addNewCostToList").click(function() {
	$(this).parent().parent().siblings().fadeIn();
	addNewCost(this);
});
  
/* Create Functions */

 function newElement(){
  var newCard = document.createElement("div");
  var newHeader = document.createElement("div");
  var newBody = document.createElement("div");
  var newPara = document.createElement("p");
  var openTicket= document.createElement("button");
  var addNew = document.createElement("button");
  var btnOT = document.createElement("i");
  var btnAN = document.createElement("i");
  var list = document.createElement("ul");
  var listItem01 = document.createElement("li");
  var listItem02 = document.createElement("li");
  var listItem03 = document.createElement("li");  
  
  $(newCard).addClass("card");
  $(newHeader).addClass("headerOfSpecificExpensesTicket");
  $(newBody).addClass("body");
  $(newPara).addClass("headerContent");
  $(openTicket).addClass("openTicket");
  $(addNew).addClass("addNewCostToList");
  $(btnOT).addClass("fa fa-toggle-down");
  $(btnAN).addClass("fa fa-plus-square-o");
  
  $(".categoryContainer").append(newCard);
  newCard.appendChild(newHeader);
  newCard.appendChild(newBody);
  newHeader.appendChild(newPara);
  newPara.appendChild(openTicket);
  newPara.appendChild(addNew);
  openTicket.appendChild(btnOT);
  addNew.appendChild(btnAN);
  newBody.appendChild(list);
  list.appendChild(listItem01);
  list.appendChild(listItem02);
  list.appendChild(listItem03);
 };
 
 function addNewCost(buttonX){
  var newItem = document.createElement("li");
  var inputValue = document.getElementById("newCost01").value;
  
  var t = document.createTextNode(inputValue + 'Ft');

  var newButton = document.createElement("button");
  var newConfig = document.createElement("i");
  
  var now = new Date();
  var today = '   '+ now.getFullYear()  + '-' + (now.getMonth() + 1) + '-' + now.getDate();
  var d = document.createTextNode(today);
  
  newItem.appendChild(t);
  newItem.appendChild(d);
  newItem.appendChild(newButton);
  newButton.appendChild(newConfig);
  
  $(newConfig).addClass("fa fa-gear");
  
  
  if (inputValue === '') {
    alert("Sum is missing!");
  } else {
	var x = $(buttonX).parent().parent().siblings().children();
	$(newItem).prependTo(x);
  }
}
